// By MoCatz_2015
//-- Slicer d'images
import java.io.File; // C'est cette fonction JAVA qui permet d'ouvrir une boite de dialog pour ouvrir un doc depuis le disque
PImage imgImport, imgSliced; // on crée une variable image ou l'on stokera l'image importée
int marge; //-- Marge our laligne
int sliceSize;
boolean drawIt;

void setup()
{
  size( 640, 480 );
  frameRate(5);
  marge = 25;
  stroke(255);
  sliceSize = 20;
  drawIt = false;
   background(60);
    text("• Appuyer sur i pour importer une image", 20, 20);
    line(0, marge, width, marge);

    if ( imgImport != null ) // ici on dit que SI ma variable image n'est pas vide (NULL) alors...
    {
      image( imgImport, 0, 30); // je l'affiche
    }
}

void draw()
{
  if (drawIt)
  {
    sliceSize = mouseY;
    background(255);
    for (int y = 0; y < 480; y += sliceSize)
    {
      copy(imgImport, 0, y, width, sliceSize, int(random(mouseX)), y, width, sliceSize);
    }
    //drawIt = false;
  } 
  else
  {
   
  }
}

void choisirUneImage( File f ) // fonction pour avoir le chemin de l'image à charger
{
  if ( f.exists() )
  {
    imgImport = loadImage( f.getAbsolutePath() );
  }
}

void keyPressed()
{
  if (key == 'i')
  {
    selectInput( "Select an image", "choisirUneImage" ); // selectInput( titre de l'action, nomde la fonction)
  }
}

void mousePressed()
{
  drawIt = true;
}
